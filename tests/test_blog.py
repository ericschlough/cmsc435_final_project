import pytest

from flaskr.db import get_db


@pytest.mark.parametrize("path", ("/create", "/1/update", "/1/delete"))
def test_login_required(client, path):
    response = client.post(path)
    assert response.headers["Location"] == "http://localhost/auth/login"


def test_author_required(app, client, auth):
    # change the post author to another user
    with app.app_context():
        db = get_db()
        db.execute("UPDATE post SET author_id = 2 WHERE id = 1")
        db.commit()

    client.post("/auth/login", data={"username": "prof", "password": "pass"})

    # current user can't modify other user's post
    assert client.post("/1/update").status_code == 403
    assert client.post("/1/delete").status_code == 403
    # current user doesn't see edit link
    assert b'href="/1/update"' not in client.get("/").data


@pytest.mark.parametrize("path", ("/2/update", "/2/delete"))
def test_exists_required(client, auth, path):
    client.post("/auth/login", data={"username": "prof", "password": "pass"})
    assert client.post(path).status_code == 404


def test_create(client, auth, app):
    client.post("/auth/login", data={"username": "prof", "password": "pass"})
    assert client.get("/create").status_code == 200
    #client.post("/create", data={"title": "created", "body": ""})
    client.post("/create", data={"title": "first", "body": "1", "class_name": "cmsc123", "username": "user"})

    with app.app_context():
        db = get_db()
        count = db.execute("SELECT COUNT(id) FROM post").fetchone()[0]
        assert count == 2


def test_update(client, auth, app):
    client.post("/auth/login", data={"username": "prof", "password": "pass"})
    assert client.get("/1/update").status_code == 200
    client.post("/1/update", data={"title": "updated", "body": ""})

    with app.app_context():
        db = get_db()
        post = db.execute("SELECT * FROM post WHERE id = 1").fetchone()
        assert post["title"] == "updated"


@pytest.mark.parametrize("path", ("/create", "/1/update"))
def test_create_update_validate(client, auth, path):
    client.post("/auth/login", data={"username": "prof", "password": "pass"})
    response = client.post(path, data={"title": "", "body": "", "class_name": ""})
    assert b"Title is required." in response.data


def test_delete(client, auth, app):
    client.post("/auth/login", data={"username": "prof", "password": "pass"})
    response = client.post("/1/delete")
    print(response)

    with app.app_context():
        assert (
            get_db().execute("SELECT * FROM post WHERE id = 1").fetchone()
            is None
        )


def test_account(client, auth, app):
    client.post("/auth/register", data={"firstname": "a", "lastname": "a", "professor": "FALSE",
                                        "email": "a@umd.edu", "username": "a", "password": "a"})
    client.post("/auth/login", data={"username": "a", "password": "a"})
    client.post("/account", data={"changeUser": "b", "changePass": "c"})
    client.post("/account")
    with app.app_context():
       assert (
           get_db().execute("select * from user where username = 'b'").fetchone()
           is not None
       )


def test_deleteAcc(client, auth, app):
    client.post("/auth/register", data={"firstname": "a", "lastname": "a", "professor": "FALSE",
                                        "email": "a@umd.edu", "username": "a", "password": "a"})
    client.post("/auth/login", data={"username": "a", "password": "a"})
    response = client.post("/deleteAcc")
    print(response)
    with app.app_context():
        assert (
                get_db().execute("select * from user where username = 'a'").fetchone()
                is None
        )
