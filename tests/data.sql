INSERT INTO user (username, password, first, last, email, professor)
VALUES
  ('prof', 'pbkdf2:sha256:150000$xFTpeKLb$24cc76f0833369975904abdaf549ff46d480dc5a0e18494ace1954030d9dad2c', 'Professor', 'Dave', 'prof@umd.edu', 'TRUE'),
  ('stu', 'pbkdf2:sha256:150000$xFTpeKLb$24cc76f0833369975904abdaf549ff46d480dc5a0e18494ace1954030d9dad2c', 'Student', 'Alexis', 'stu@umd.edu', 'FALSE');

INSERT INTO post (title, body, author_id, created, class_name, submission, username)
VALUES
    ("hwTest", "bodyTest", 1, "2018-01-01 00:00:00", "cmsc123", "FALSE", "prof");

INSERT INTO classes (name, class_id, username)
VALUES
    ("cmsc123", 123, "prof");
