import os

import pytest
from flask import g
from flask import session

from flaskr.db import get_db

def test_Scenario1(client, auth, app):
    # Test Register for Professor
    assert client.get("/auth/register").status_code == 200
    response = client.post("/auth/register", data={"firstname": "Professor2", "lastname": "Dave2", "professor": "TRUE",
                                                   "email": "prof2@umd.edu", "username": "prof2", "password": "pass2"})
    assert "http://localhost/auth/login" == response.headers["Location"]

    # Test Login for Professor
    assert client.get("/auth/login").status_code == 200
    response = client.post("/auth/login", data={"username": "prof2", "password": "pass2"})
    print(response)
    print(response.headers["Location"])

    # login request set the user_id in the session
    # check that the user is loaded from the session
    with client:
        client.get("/")
        assert session["user_id"] == 3
        assert g.user["username"] == "prof2"

    # Create a class
    client.post("/auth/professor_class", data={"label": "enee324", "body": "324"})
    # test that the class was inserted into the database
    with app.app_context():
        assert (
                get_db().execute("select * from classes where name = 'enee324'").fetchone()
                is not None
        )

    # Create an assignment
    pdf = open("tests/hw11.pdf", 'rb')
    client.post("/auth/enter_file", data={"data": pdf, "assignment": "hw11", "class_name": "enee324"})
    with app.app_context():
        assert (
                get_db().execute("select * from post where title = 'hw11'").fetchone()
                is not None
        )

def test_Scenario2(client, auth, app):
    # Test Register for Professor
    assert client.get("/auth/register").status_code == 200
    response = client.post("/auth/register", data={"firstname": "Student2", "lastname": "Alexis2", "professor": "FALSE",
                                                   "email": "stu2@umd.edu", "username": "stu2", "password": "pass"})
    assert "http://localhost/auth/login" == response.headers["Location"]

    # Test Login for Professor
    assert client.get("/auth/login").status_code == 200
    response = client.post("/auth/login", data={"username": "stu2", "password": "pass"})
    print(response)
    print(response.headers["Location"])

    # login request set the user_id in the session
    # check that the user is loaded from the session
    with client:
        client.get("/")
        assert session["user_id"] == 3
        assert g.user["username"] == "stu2"

    # Register for a class
    client.post("/auth/student_class", data={"label": "cmsc123", "body": "123"})
    # test that the class was inserted into the database
    with app.app_context():
        assert (
                get_db().execute("select * from registration where name = 'cmsc123'").fetchone()
                is not None
        )

    # submit an assignment
    pdf = open("tests/hw11.pdf", 'rb')
    client.post("/auth/submit_assignment", data={"data": pdf, "assignment": "hwTest", "class_name": "cmsc123"})
    with app.app_context():
        assert (
                get_db().execute("select * from post where title = 'hwTest'").fetchone()
                is not None
        )

def test_Scenario3(client, auth, app):
    # Student Login
    assert client.get("/auth/login").status_code == 200
    response = client.post("/auth/login", data={"username": "stu", "password": "pass"})

    with client:
        client.get("/")
        assert session["user_id"] == 2
        assert g.user["username"] == "stu"

    # Send a message
    client.post("/auth/send_messages", data={"label": "stu", "receiver": "prof", "body": "Test Message",
                                        "announcement": "FALSE"})

    # test that the message was inserted into the database
    with app.app_context():
        assert (
                get_db().execute("select * from messages where sender = 'stu'").fetchone()
                is not None
        )

    # Login as professor to view message
    assert client.get("/auth/login").status_code == 200
    client.post("/auth/login", data={"username": "prof", "password": "pass"})

    with client:
        client.get("/")
        assert session["user_id"] == 1
        assert g.user["username"] == "prof"

    # Send a message
    client.post("/auth/send_messages", data={"label": "prof", "receiver": "stu", "body": "Test Return Message",
                                             "announcement": "FALSE"})

    # test that the message was inserted into the database
    with app.app_context():
        assert (
                get_db().execute("select * from messages where sender = 'prof'").fetchone()
                is not None
        )

def test_Scenario4(client, app):\
    # Login as professor
    assert client.get("/auth/login").status_code == 200
    client.post("/auth/login", data={"username": "prof", "password": "pass"})

    with client:
        client.get("/")
        assert session["user_id"] == 1
        assert g.user["username"] == "prof"

    # Send an announcement
    client.post("/auth/send_messages", data={"label": "prof", "receiver": "cmsc123", "body": "Test Announcement Message",
                                             "announcement": "TRUE"})

    # test that the announcement was inserted into the database
    with app.app_context():
        assert (
                get_db().execute("select * from messages where sender = 'prof'").fetchone()
                is not None
        )

