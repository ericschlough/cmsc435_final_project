import os

import pytest
from flask import g
from flask import session

from flaskr.db import get_db


def test_reg_Prof(client, app):
    # Test Registration for Professor
    assert client.get("/auth/register").status_code == 200
    response = client.post("/auth/register", data={"firstname": "Professor2", "lastname": "Dave2", "professor": "TRUE",
                                                   "email": "prof2@umd.edu", "username": "prof2", "password": "pass2"})
    assert "http://localhost/auth/login" == response.headers["Location"]

    # test that the user was inserted into the database
    with app.app_context():
        assert (
                get_db().execute("select * from user where username = 'prof2'").fetchone()
                is not None
        )


def test_reg_Student(client, app):
    # Test Registration for Student
    assert client.get("/auth/register").status_code == 200
    response = client.post("/auth/register", data={"firstname": "Student2", "lastname": "Alexis2", "professor": "FALSE",
                                                   "email": "stu2@umd.edu", "username": "stu2", "password": "pass2"})
    assert "http://localhost/auth/login" == response.headers["Location"]

    # test that the user was inserted into the database
    with app.app_context():
        assert (
                get_db().execute("select * from user where username = 'stu2'").fetchone()
                is not None
        )


def test_login(client, auth):
    # test that viewing the page renders without template errors
    assert client.get("/auth/login").status_code == 200

    # test that successful login redirects to the index page
    response = client.post("/auth/login", data={"username": "prof", "password": "pass"})
    assert response.headers["Location"] == "http://localhost/"

    # login request set the user_id in the session
    # check that the user is loaded from the session
    with client:
        client.get("/")
        assert session["user_id"] == 1
        assert g.user["username"] == "prof"

@pytest.mark.parametrize(
    ("firstname", "lastname", "email", "username", "password", "message"),
    (
        ("", "test", "test", "test", "test", b"First name is required"),
        ("test", "", "test", "test", "test", b"Last name is required"),
        ("test", "test", "", "test", "test", b"email is required"),
        ("test", "test", "test", "", "test", b"Username is required."),
        ("test", "test", "test", "test", "", b"Password is required."),
        ("test", "test", "test", "prof", "test", b"User prof is already registered."),
    ),
)
def test_register_validate_input(client, firstname, lastname, email, username, password, message):
    response = client.post(
        "/auth/register",  data={"firstname": firstname, "lastname": lastname,
        "email": email, "username": username, "password": password}
    )
    assert message in response.data


@pytest.mark.parametrize(
    ("username", "password", "message"),
    (("a", "test", b"Incorrect username."), ("prof", "a", b"Incorrect password.")),
)
def test_login_validate_input(client, username, password, message):
    response = client.post("/auth/login", data={"username": username, "password": password})
    assert message in response.data


def test_logout(client, auth):
    auth.login()
    with client:
        auth.logout()
        assert "user_id" not in session

@pytest.mark.parametrize(
    ("label", "body", "message"),
    (
        ("", "a", b"Title is required."),
        ("a", "", b"Body is required."),
    ),
)
def test_manual_input_validate_input(client, label, body, message):
    client.post("/auth/login", data={"username": "stu", "password": "pass"})
    response = client.post("/auth/student_class", data={"label": label, "body": body})
    assert message in response.data

    client.post("/auth/login", data={"username": "prof", "password": "pass"})
    response = client.post("/auth/professor_class", data={"label": label, "body": body})
    assert message in response.data

