FROM python:3.7

WORKDIR /app
COPY . /app
RUN pip install -e .
RUN pip install -r requirements.txt

ENV FLASK_APP=flaskr
ENV FLASK_ENV=development
RUN ["flask", "init-db"]
CMD ["gunicorn", "-w", "4", "--bind", "0.0.0.0:5000", "--reload", "wsgi:app"]
