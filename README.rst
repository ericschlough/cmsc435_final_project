![coverage](https://cmsc435.garrettvanhoy.com/samisiddiqui/final-project/edit/master/coverage.svg?style=flat-square)

Team 3
======

Final-Project: Canvas2
======================

This README explains how to install and run our Canvas like program.

AWS
---
Our program is hosted on EC2, access utilizing this link

    http://52.34.203.57:5000/

Install
-------

**Docker** is utilized for building and running the code simplifying the installation process!

Clone the repository::

    $ git clone http://cmsc435.garrettvanhoy.com/samisiddiqui/final-project.git
    $ cd final-project

Create Docker image/container utilizing docker commands::

    $ docker-compose build
    $ docker-compose up

The server will be setup on the localhost of the docker container so you will need to know it's ip address

Container's ip address::

    $ docker-machine ip

Open http://localhost:5000 in a browser. (localhost of docker container)

If you would like to see our code coverage of over 90%, please utilize these commands
By covering the majority of our code we cover the code utilized in our User Stories

Test
----

::

    $ pip install '.[test]'
    $ pytest

Run with coverage report::

    $ coverage run -m pytest
    $ coverage report
    $ coverage html  # open htmlcov/index.html in a browser
    
Application Functionality
=========================

__init__.py
-----------

    This file initializes the Flask app and sets up some initial configuration. 
    The database is initialized using a function call to the db.py file.
    Blueprints are also stored to be used for generating HTML pages for unique
    classes.

db.py
-----

    This file includes multiple functions to initialize and manage the database.
    These tasks include registering the database with the Flask app, clearing 
    existing data, creating new tables, fetching the app's configured database, 
    and closing the connection to the database.

schema.sql
----------

    This sql file defines all of the tables for our database. The user table holds
    all students and professors. Their username, password, and personal 
    information is all included. The post table holds posts which can be viewed 
    publicly by anyone in the particluar class. The classes table stores all
    available classes and links their names with their ids. The registration
    table stores the classes that each student is registered for. The messages 
    table holds private messages between two users using
    the application.

auth.py
-------

login_required
::

    Called when a user must be logged in to access a page. If no user is logged
    in, it will redirect to the login page.

load_logged_in_user
::

    Called to fetch the current user that is stored in the session from the 
    database.


register
::

    Called to insert a new unique user into the database.

login
::

    Called to log in a registered user. This is done by fetching the user from 
    the database and adding their id to the session.

file
::

    Called to upload a file to the server storage.

submit_assignment
::

    Called to upload an assignment (file) to the server storage.

serve_static
::

    A helper method to find the correct directory for file uploads on the server.

createfromcsv
::

    Called to create a new post for the current user. The data is inserted into
    the database to be displayed on the page.

home
::

    Called to redirect to the data display page.

logout
::

    Called to clear the current session and effectively log out the user.

send_messages
::

    Called to send a message from a sender to a receiver. The sender, receiver,
    and message text are stored in the database where it will be fetched to be 
    displayed for the two appropriate users.

professor_class
::

    Called to create a new class. The new class is inserted into the classes
    table. Can only be done when the user in the session is a professor.

student_class
::

    Called to register a student for a class. The relationship is added to the
    registration table.

blog.py
-------

view_messages
::

    Called to fetch and dispaly all messages and announcements from the messages
    table.

view_submissions
::

    Called to fetch all posts from the post table that are associated with the
    current user's classes.


view_inbox
::

    Called to fetch and display a user's sent and received messages from the
    messages table. In addition, class announcements are displayed in the inbox.
    When a professor creates a new assignment, an announcement is automatically
    sent out to the students of that class.

index
::

    Called to fetch and display the main page of the application. The current
    user's classes and their posts are displayed.

search
::

    Called to search parse the posts and find results that match the user-provided
    filter.

get_post
::

    Called to get a post from the post table that matches a provided id.

create
::

    Called to create a new post in-browser for the current user for a specific class that
    they are enrolled in.

createfromcsv
::

    Called to create a new post from a file for the current user for a specific 
    class that they are enrolled in.

update
::

    Called to update an existing post if the current user id matches the post's
    author id.

delete
::

    Called to delete an existing post if the current user id matches the post's
    author id.

deleteAcc
::

    Called to delete a user account. The session is cleared, all relevant data
    is cleared from the post and user tables.

account
::

    Called to manage the current user's account. Both the user's username and
    password can be changed.