-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS post;
DROP TABLE IF EXISTS classes;
DROP TABLE IF EXISTS registration;
DROP TABLE IF EXISTS messages;

CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL,
  first TEXT,
  last TEXT,
  email TEXT,
  professor BIT default 'FALSE'
);

CREATE TABLE post (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  author_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  title TEXT NOT NULL,
  body TEXT NOT NULL,
  class_name TEXT NOT NULL,
  submission BIT default 'FALSE',
  username TEXT NOT NULL,
  FOREIGN KEY (username) REFERENCES user (username),
  FOREIGN KEY (author_id) REFERENCES user (id)
);

CREATE TABLE classes (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT UNIQUE,
  class_id INTEGER UNIQUE,
  username TEXT NOT NULL,
  FOREIGN KEY (username) REFERENCES user (username)
);

CREATE TABLE registration (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL,
  username TEXT NOT NULL,
  FOREIGN KEY (username) REFERENCES user (username)
);

CREATE TABLE messages (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  sender TEXT NOT NULL,
  receiver TEXT NOT NULL,
  message TEXT,
  announcement BIT default 'FALSE'
);



