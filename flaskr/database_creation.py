#from flask import Flask
##from flask_sqlalchemy import SQLAlchemy

##app = Flask(__name__)
##app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///canvas.db'
##db = SQLAlchemy(app)

##association table student->classes
# enroll = db.Table('enroll',
    # db.Column('stud_id', db.Integer, db.ForeignKey('student.id')),
    # db.Column('class_id', db.Integer, db.ForeignKey('classes.id'))
    # )

##association table classes->students
# enrolled_to = db.Table('enrolled_to',
    # db.Column('class_id', db.Integer, db.ForeignKey('classes.id')),
    # db.Column('stud_id', db.Integer, db.ForeignKey('student.id'))
    # )
    
##association table student->classes
# assigned = db.Table('assigned',
    # db.Column('stud_id', db.Integer, db.ForeignKey('student.id')),
    # db.Column('assign_id', db.Integer, db.ForeignKey('assignments.id'))
    # )

##association table classes->students
# assigned_to = db.Table('assigned_to',
    # db.Column('assign_id', db.Integer, db.ForeignKey('assignments.id')),
    # db.Column('stud_id', db.Integer, db.ForeignKey('student.id'))
    # )

# class Professor(db.Model):
    # id = db.Column(db.Integer, primary_key=True)
    # name = db.Column(db.String(30))
    ##(classes object).professor
    # classes = db.relationship('Classes', backref='professor')
    ##(assignment object).professor
    # assignments = db.relationship('Assignments', backref='professor')    
    
# class Classes(db.Model):
    # id = db.Column(db.Integer, primary_key=True)
    # name = db.Column(db.String(30))
    # prof_id = db.Column(db.Integer, db.ForeignKey('professor.id')) #foreign key
    ##(assignment object).class
    # assignments = db.relationship('Assignments', backref='get_class')
    ##(student object).classes
    # enrolled_students = db.relationship('Student', 
        # secondary=enrolled_to, 
        # backref=db.backref('classes', lazy='dynamic')
        # )

# class Student(db.Model):
    # id = db.Column(db.Integer, primary_key=True)
    # name = db.Column(db.String(30))
    ##(class object).students
    # enrollments = db.relationship('Classes', 
        # secondary=enroll, 
        # backref=db.backref('students', lazy='dynamic')
        # )
    ##(assignment obj).assignees
    # assignments = db.relationship('Assignments', 
        # secondary=assigned, 
        # backref=db.backref('get_assigned', lazy='dynamic')
        # )
        
##holds all assignments
# class Assignments(db.Model):
    # id = db.Column(db.Integer, primary_key=True)
    # name = db.Column(db.String(30))
    # prof_id = db.Column(db.Integer, db.ForeignKey('professor.id')) #foreign key
    # class_id = db.Column(db.Integer, db.ForeignKey('classes.id')) #foreign key
    # desc = db.Column(db.Text)
    ##(student obj).assignments
    # assignees = db.relationship('Student', 
        # secondary=assigned_to, 
        # backref=db.backref('get_assignments', lazy='dynamic')
        # )
    