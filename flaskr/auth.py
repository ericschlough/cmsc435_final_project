import functools
import os
import csv
import webbrowser
from flask import Flask
from flask import Blueprint
from flask import flash
from flask import g
from flask import redirect
from flask import render_template, current_app
from flask import request
from flask import session
from flask import url_for
from werkzeug.security import check_password_hash
from werkzeug.security import generate_password_hash
from werkzeug.utils import secure_filename
from flask import send_from_directory, current_app as app

from flaskr.db import get_db

app = Flask(__name__)
UPLOAD_FOLDER = '/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER 

bp = Blueprint("auth", __name__, static_folder='static', url_prefix="/auth")


def login_required(view):
    """View decorator that redirects anonymous users to the login page."""

    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for("auth.login"))

        return view(**kwargs)

    return wrapped_view


@bp.before_app_request
def load_logged_in_user():
    """If a user id is stored in the session, load the user object from
    the database into ``g.user``."""
    user_id = session.get("user_id")

    if user_id is None:
        g.user = None
    else:
        g.user = (
            get_db().execute("SELECT * FROM user WHERE id = ?", (user_id,)).fetchone()
        )


@bp.route("/register", methods=("GET", "POST"))
def register():
    """Register a new user.

    Validates that the username is not already taken. Hashes the
    password for security.
    """
    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]
        email = request.form["email"]
        first_name = request.form["firstname"]
        last_name = request.form["lastname"]
        prof = "professor" in request.form
        db = get_db()
        error = None

        if not username:
            error = "Username is required."
        elif not password:
            error = "Password is required."
        elif not first_name: 
            error = "First name is required"
        elif not last_name: 
            error = "Last name is required"
        elif not email: 
            error = "email is required"
        elif (
            db.execute("SELECT id FROM user WHERE username = ?", (username,)).fetchone()
            is not None
        ):
            error = "User {0} is already registered.".format(username)

        if error is None:
            # the name is available, store it in the database and go to
            # the login page
            db.execute(
                "INSERT INTO user (username, password, first, last, email, professor) VALUES (?, ?, ?, ?, ?, ?)",
                (username, generate_password_hash(password), first_name, last_name, email, prof),
            )
           # db.execute(
            #    "INSERT INTO user (first, last, email) VALUES (?, ?, ?)",
             #   (first_name, last_name, email),
            #)
            db.commit()
            return redirect(url_for("auth.login"))

        flash(error)

    return render_template("auth/register.html")


@bp.route("/login", methods=("GET", "POST"))
def login():
    """Log in a registered user by adding the user id to the session."""
    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]
        db = get_db()
        error = None
        user = db.execute(
            "SELECT * FROM user WHERE username = ?", (username,)
        ).fetchone()

        if user is None:
            error = "Incorrect username."
        elif not check_password_hash(user["password"], password):
            error = "Incorrect password."

        if error is None:
            # store the user id in a new session and return to the index
            session.clear()
            session["user_id"] = user["id"]
            return redirect(url_for("index"))

        flash(error)

    return render_template("auth/login.html")

@bp.route("/enter_file", methods=("GET", "POST"))
def file():
    """Enter a file when logged in."""
    if request.method == "POST":
        file = request.files['data']
        assignment = request.form["assignment"]
        class_name = request.form["class_name"]
       
        filename = secure_filename(file.filename)
        print("filename is")
        print(filename)
        createfromcsv(assignment,filename,class_name,0)
        root_dir = os.path.dirname(os.getcwd())
        
        file.save(os.path.join(root_dir, filename))
        #file.save(os.path.join(root_dir, 'final-project', 'flaskr', 'static'))
        serve_static(filename)
        #send_from_directory('C:/Users/erics/Desktop', 'file.pdf')
        flash('File successfully uploaded')
        #input_file = csv.DictReader(open(filename))
        x = []
        i = 1

        db = get_db()
        error = None

    return render_template("auth/file_enter.html")

@bp.route("/submit_assignment", methods=("GET", "POST"))
def submit_assignment():
    """Enter a file when logged in."""
    if request.method == "POST":
        file = request.files['data']
        assignment = request.form["assignment"]
        class_name = request.form["class_name"]

        filename = secure_filename(file.filename)
        print("filename is")
        print(filename)
        createfromcsv(assignment, filename, class_name, 1)
        root_dir = os.path.dirname(os.getcwd())

        file.save(os.path.join(root_dir, filename))
        # file.save(os.path.join(root_dir, 'final-project', 'flaskr', 'static'))
        serve_static(filename)
        # send_from_directory('C:/Users/erics/Desktop', 'file.pdf')
        flash('File successfully uploaded')
        # input_file = csv.DictReader(open(filename))
        x = []
        i = 1

        db = get_db()
        error = None

    return render_template("auth/file_enter.html")

@bp.route('/results/<path:filename>', methods=['GET'])
def serve_static(filename):
    root_dir = os.path.dirname(os.getcwd())
    uploads = os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'])
    return send_from_directory(root_dir, filename)


def createfromcsv(label,data,class_name,submission):
    """Create a new post for the current user."""
    if request.method == "POST":
        title = label
        body = data
        error = None

        db = get_db()
        db.execute(
            "INSERT INTO post (title, body, author_id, class_name, submission, username) VALUES (?, ?, ?, ?, ?, ?)",
            (title, body, g.user["id"], class_name, submission, g.user["username"]),
        )
        db.commit()
        return redirect(url_for("blog.index"))

    return render_template("blog/create.html")


def home():
    """Return to the data display page"""
    return redirect(url_for("blog.index"))


@bp.route("/logout")
def logout():
    """Clear the current session, including the stored user id."""
    session.clear()
    return redirect(url_for("auth.login"))

@bp.route("/send_messages", methods=("GET", "POST"))
def send_messages():
    """Enter a file when logged in."""
    if request.method == "POST":
        sender = request.form["label"]
        receiver = request.form["receiver"]
        message = request.form["body"]
        announcement = "announcement" in request.form
        error = None

        if not sender:
            error = "Title is required."

        if not message:
            error = "Body is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "INSERT INTO messages (sender, receiver, message, announcement) VALUES (?, ?, ?,?)",
                (sender, receiver, message, announcement)
            )
            db.commit()
            return redirect(url_for("blog.index"))
        error = None

    return render_template("auth/send_messages.html")


def send_notification(class_name,assignment_name):
    message = class_name + ": Instructor posted " + assignment_name
    db = get_db()
    db.execute(
        "INSERT INTO messages (sender, receiver, message, announcement) VALUES (?, ?, ?,?)",
        (g.user["username"], class_name, message, 1)
    )
    db.commit()


@bp.route("/professor_class", methods=("GET", "POST"))
def professor_class():
    """Enter a file when logged in."""
    if request.method == "POST":
        label = request.form["label"]
        data = request.form["body"]
        error = None
        print("username is: ")
        print(g.user["username"])
        if not label:
            error = "Title is required."

        if not data:
            error = "Body is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "INSERT INTO classes (name, class_id, username) VALUES (?, ?, ?)",
                (label, data, g.user["username"])
            )
            db.commit()
            return redirect(url_for("blog.index"))
        error = None

    return render_template("auth/manual_input.html")

@bp.route("/student_class", methods=("GET", "POST"))
def student_class():
    """Enter a file when logged in."""
    if request.method == "POST":
        label = request.form["label"]
        data = request.form["body"]
        error = None

        if not label:
            error = "Title is required."

        if not data:
            error = "Body is required."

        if error is not None:
            flash(error)
        else:
            class_name = (
                get_db()
                    .execute(
                    "SELECT name FROM classes WHERE class_id=?",
                    (data,)
                ).fetchone()
            )
            print(class_name[0])
            if class_name[0] == label:
                db = get_db()
                db.execute(
                    "INSERT INTO registration (name, username) VALUES (?, ?)",
                    (label, g.user["username"])
                )
                db.commit()
            return redirect(url_for("blog.index"))
        error = None

    return render_template("auth/manual_input.html")
