from flask import Blueprint
from flask import flash
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from flask import make_response
from flask import Response
from werkzeug.exceptions import abort
from werkzeug.security import generate_password_hash
import webbrowser

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint("blog", __name__)


@bp.route("/view_messages")
def view_messages():
    """Show all the posts, most recent first."""
    db = get_db()

    # messages = db.execute(
    #     "SELECT receiver"
    #     " FROM messages"
    # ).fetchone()
    #
    # students = db.execute(
    #     "SELECT username"
    #     " FROM registration"
    #     "WHERE name=?",
    #     ()
    # ).fetchall()

    messages = db.execute(
        "SELECT sender, receiver, message, announcement"
        " FROM messages"
    ).fetchall()

    return render_template("auth/view_messages.html", messages=messages)

@bp.route("/view_submissions")
def view_submissions():
    """Show all the posts, most recent first."""
    db = get_db()

    posts = db.execute(
        "SELECT id, title, body, created, author_id, class_name, submission, username"
        " FROM post "
    ).fetchall()

    registered = (
        get_db()
            .execute(
            "SELECT name FROM classes WHERE username=?",
            (g.user["username"],)
        ).fetchall()
    )

    return render_template("auth/view_submissions.html", posts=posts, registered=registered)

@bp.route("/view_inbox")
def view_inbox():
    """Show all the posts, most recent first."""
    db = get_db()

    # classes = db.execute(
    #     "SELECT name"
    #     " FROM registration"
    #     "WHERE username=?",
    #     (g.user["id"]),
    # ).fetchall()

    classes = (
        get_db()
            .execute(
            "SELECT name FROM registration WHERE username=?",
            (g.user["username"],)
        ).fetchall()
    )

    registered = (
        get_db()
            .execute(
            "SELECT name FROM classes WHERE username=?",
            (g.user["username"],)
        ).fetchall()
    )

    print(g.user["username"])

    messages = db.execute(
        "SELECT sender, receiver, message, announcement"
        " FROM messages"
    ).fetchall()

    return render_template("auth/view_inbox.html", messages=messages, classes=classes, registered=registered)


@bp.route("/")
def index():
    """Show all the posts, most recent first."""      
    db = get_db()

    # Checks to make sure no one is logged in and returns blank screen
    if g.user is None:
        return render_template("blog/index.html")

    posts = db.execute(
        "SELECT p.id, title, body, created, author_id, class_name, submission"
        " FROM post p JOIN user u ON p.author_id = u.id"
        " ORDER BY created DESC"
    ).fetchall()

    classes = (
        get_db()
            .execute(
            "SELECT name FROM registration WHERE username=?",
            (g.user["username"],)
        ).fetchall()
    )

    registered = (
        get_db()
            .execute(
            "SELECT name FROM classes WHERE username=?",
            (g.user["username"],)
        ).fetchall()
    )

    return render_template("blog/index.html", posts=posts, classes=classes, registered=registered)


@bp.route("/search", methods=("GET", "POST"))
def search():
    """Search for term
    """
    if request.method == "POST":
        searchLabel = request.form["search"]
        
        db = get_db()

        posts = db.execute(
        "SELECT p.id, title, body, created, author_id, username, class_name"
        " FROM post p JOIN user u ON p.author_id = u.id"
        " WHERE title = ?", (searchLabel,)
        ).fetchall()
        return render_template("blog/index.html", posts=posts)

    return render_template("auth/register.html")    
    
def get_post(id, check_author=True):
    """Get a post and its author by id.

    Checks that the id exists and optionally that the current user is
    the author.

    :param id: id of post to get
    :param check_author: require the current user to be the author
    :return: the post with author information
    :raise 404: if a post with the given id doesn't exist
    :raise 403: if the current user isn't the author
    """
    post = (
        get_db()
        .execute(
            "SELECT p.id, title, body, created, author_id"
            " FROM post p JOIN user u ON p.author_id = u.id"
            " WHERE p.id = ?",
            (id,),
        )
        .fetchone()
    )

    if post is None:
        abort(404, "Post id {0} doesn't exist.".format(id))

    if check_author and post["author_id"] != g.user["id"]:
        abort(403)

    return post


@bp.route("/create", methods=("GET", "POST"))
@login_required
def create():
    """Create a new post for the current user."""
    if request.method == "POST":
        title = request.form["title"]
        body = request.form["body"]
        class_name = request.form["class_name"]
        error = None

        if not title:
            error = "Title is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "INSERT INTO post (title, body, author_id, class_name, username) VALUES (?, ?, ?, ?, ?)",
                (title, body, g.user["id"], class_name, g.user["username"]),
            )
            db.commit()
            return redirect(url_for("blog.index"))

    return render_template("blog/create.html")

@bp.route("/create", methods=("GET", "POST"))
@login_required
def createfromcsv(label,data):
    """Create a new post for the current user."""
    if request.method == "POST":
        title = label
        body = data
        error = None

        if not title:
            error = "Title is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "INSERT INTO post (title, body, author_id) VALUES (?, ?, ?)",
                (title, body, g.user["id"]),
            )
            db.commit()
            return redirect(url_for("blog.index"))

    return render_template("blog/create.html")


@bp.route("/<int:id>/update", methods=("GET", "POST"))
@login_required
def update(id):
    """Update a post if the current user is the author."""
    post = get_post(id)

    if request.method == "POST":
        title = request.form["title"]
        body = request.form["body"]
        error = None

        if not title:
            error = "Title is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "UPDATE post SET title = ?, body = ? WHERE id = ?", (title, body, id)
            )
            db.commit()
            return redirect(url_for("blog.index"))

        webbrowser.open(body)

    return render_template("blog/update.html", post=post)

@bp.route("/<int:id>/delete", methods=("POST",))
@login_required
def delete(id):
    """Delete a post.

    Ensures that the post exists and that the logged in user is the
    author of the post.
    """
    get_post(id)
    db = get_db()
    db.execute("DELETE FROM post WHERE id = ?", (id,))
    db.commit()
    return redirect(url_for("blog.index"))

@bp.route("/deleteAcc", methods=("GET", "POST"))
@login_required
def deleteAcc():
    """Clear the current session, including the stored user id."""
    db = get_db()
    error = None
        
    db.execute(
        "DELETE FROM post WHERE author_id = ?", (g.user["id"],)
    )
    
    db.execute(
        "DELETE FROM user WHERE id = ?", (g.user["id"],)
    ) 
    db.commit()    

    flash(error)
    
    return redirect(url_for("auth.logout"))    
    
@bp.route("/account", methods=("GET", "POST"))
@login_required
def account():
    if request.method == "POST":
        db = get_db()
        error = None
        if "changeUser" in request.form:
            username = request.form["changeUser"]
            if(username != ""):
                db.execute(
                    "UPDATE user SET username = ? WHERE id = ?", (username, g.user["id"])
                )
                db.commit()
        if "changePass" in request.form:
            password = request.form["changePass"]
            if(password != ""):
                db.execute(
                    "UPDATE user SET password = ? WHERE id = ?", (generate_password_hash(password), g.user["id"])
                )
                db.commit()
        
        if error is None:
            # store the user id in a new session and return to the index
            return redirect(url_for("blog.account"))

        flash(error)
        
    return render_template("blog/account.html")